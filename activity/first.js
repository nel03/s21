// console.log('hi')

let drinkName = ['Gin', 'Empe', 'Beer', 'Light'];
console.log(drinkName);

function addDrink(newDrink){
	drinkName[drinkName.length++] = newDrink;
};

addDrink('Double Light');
addDrink('Red Wine');
console.log('Added new Drinks for the Day:');
console.log(drinkName);


function getDrinkByIndex(index){
	return drinkName[index];
};

let addNewDrink = getDrinkByIndex(2);
console.log(addNewDrink);

function removedDrink(){
	let lastDrink = drinkName[drinkName.length-1];
	drinkName.pop();
	return lastDrink;
};

lastDrink = removedDrink();
console.log('Drink that was removed: ' + lastDrink)
console.log(drinkName);

function updateDrink(index, drinks) {
	drinkName[index] = drinks;
};

updateDrink(1, 'Double Black');
console.log('Update the Drinks due to availability:')
console.log(drinkName);

function deleteDrinks(){
	drinkName = [];
};

deleteDrinks();
console.log('Sorry all drinks are sold:');
console.log(drinkName);

function checkDrinkItem(){
	if (drinkName.length > 0){
		return false;
	}else {
		return true;
	};
}

let isDrinkSold = checkDrinkItem();
console.log('Are all the Drinks Sold?')
console.log(isDrinkSold);